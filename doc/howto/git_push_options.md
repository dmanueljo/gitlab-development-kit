---
stage: Ecosystem
group: Contributor Experience
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Enabling Git push options

To support Git [push options](https://docs.gitlab.com/ee/user/project/push_options.html)
with GDK, run the following command:

```shell
git config --global receive.advertisepushoptions true
```
